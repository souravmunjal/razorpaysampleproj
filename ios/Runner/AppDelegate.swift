import UIKit
import Flutter
import Razorpay

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var eventSink: FlutterEventSink?
     var razorpay: Razorpay!
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
     razorpay = Razorpay.initWithKey("rzp_test_rtdw3pxrAaZvxO", andDelegate: self)
    let batteryChannel = FlutterMethodChannel(name: "samples.flutter.dev/battery",
                                              binaryMessenger: controller.binaryMessenger)
    batteryChannel.setMethodCallHandler({
      [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
      // Note: this method is invoked on the UI thread.
      guard call.method == "getBatteryLevel" else {
         if let param = call.arguments as? [String: Any] {
                  
            print("method call",param)
            self?.showPaymentForm(param: param, vc: controller)
            }
        return
      }
       
        if let param = call.arguments as? [String: Any] {
                 
           print("method call",param)
           self?.showPaymentForm(param: param, vc: controller)
           }

      self?.receiveBatteryLevel(result: result)
    })

    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    func showPaymentForm(param: [String:Any], vc: FlutterViewController){
              let options: [String:Any] = [
                          "amount": "100.0", //This is in currency subunits. 100 = 100 paise= INR 1.
                          "currency": "INR",//We support more that 92 international currencies.
                          "description": "purchase description",
                          "image": "https://url-to-image.png",
                          "name": "business or product name",
                          "prefill": [
                              "contact": "9960725240",
                              "email": "raj.kadam@stylabs.in"
                          ],
                          "theme": [
                              "color": "#528FF0"
                          ]
                      ]
              razorpay.open(options, display: vc)
          }
    
  private func receiveBatteryLevel(result: FlutterResult) {
    let device = UIDevice.current
    device.isBatteryMonitoringEnabled = true
    if device.batteryState == UIDevice.BatteryState.unknown {
      result(FlutterError(code: "UNAVAILABLE",
                          message: "Battery info unavailable",
                          details: nil))
    } else {
      result(Int(device.batteryLevel * 100))
    }
  }
    
    public func onListen(withArguments arguments: Any?, eventSink events: @escaping FlutterEventSink) -> FlutterError? {
               eventSink = events

               return nil
           }
           
           public func onCancel(withArguments arguments: Any?) -> FlutterError? {
               eventSink = nil
               return nil
           }
           
     public func sendPaymentDataEvent(data: Dictionary<String, Any>) {
               if eventSink == nil {
                   return
               }

               eventSink!(data)
           }
       

}

extension AppDelegate : RazorpayPaymentCompletionProtocolWithData, RazorpayPaymentCompletionProtocol {
    
    
    public func onPaymentError(_ code: Int32, description str: String) {
        
        print("error",str)
         print("error: ", code, str)
    }
    
    public func onPaymentSuccess(_ payment_id: String) {
        print("success",payment_id)
    }
    
    
    public func onPaymentError(_ code: Int32, description str: String, andData response: [AnyHashable : Any]?) {
        print("error: ", code)
    }
    
    public func onPaymentSuccess(_ payment_id: String, andData response: [AnyHashable : Any]?) {
        print("success: ", payment_id)
        
        self.sendPaymentDataEvent(data: ["payment_id":payment_id])
    }
    
    
}
