import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  String batteryLevel = "xxxxx";
  static const platform = const MethodChannel('samples.flutter.dev/battery');
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () async {
                String batteryLevel2;
                String result;
                var options = {
                  'key': "rzp_test_rtdw3pxrAaZvxO",
                  'amount': 2000,
                };

                try {
                  //if (Platform.isAndroid)
                    result = (await platform.invokeMethod(
                            'getBatteryLevel', options))
                        .toString();
                  //else
                    //result = (await platform.invokeMethod('getBatteryLevel')).toString();
                  batteryLevel2 = 'Battery level at $result % .';
                  print(batteryLevel2.toString() + "custom implemented");
                  setState(() {
                    batteryLevel = batteryLevel2;
                  });
                } on PlatformException catch (e) {
                  batteryLevel2 =
                      "Failed to get battery level: '${e.message}'.";
                  setState(() {
                    batteryLevel = batteryLevel2;
                  });
                }
              },
              child: Text("Press The button"),
            ),
            Text(
              batteryLevel,
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
    );
  }
}
